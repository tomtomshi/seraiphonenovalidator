import React from 'react';
import { Table } from 'antd';

interface TableProps {
  columns: any;
  data: any;
  rowKey: string;
}

export function CustomTable(props: TableProps) {
  const { columns, data, rowKey } = props;
  return <Table columns={columns} rowKey={rowKey} dataSource={data} />;
}
