import React from 'react';
import './App.css';

import PhoneStore from './stores/PhoneStore';
import ValidationPage from './pages/validation';

import { Layout } from 'antd';
const { Header, Content, Footer } = Layout;

function App() {
  const store = new PhoneStore();

  return (
    <Layout className='layout'>
      <Content
        style={{ padding: '50px 50px', margin: '0 auto', minWidth: 1000 }}
      >
        Serai Phone Number Validation
        <div className='App'>
          <ValidationPage store={store} />
        </div>
      </Content>
      <Footer />
    </Layout>
  );
}

export default App;
