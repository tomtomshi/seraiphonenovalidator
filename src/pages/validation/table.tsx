import React from 'react';
import { observer } from 'mobx-react';

// Component
import { CustomTable } from '../../components/Tables';

// Style
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';

const ValidationTable = observer(({ store }: any) => {
  const columns: Array<Object> = [

    {
      title: 'Phone Number',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Correct',
      dataIndex: 'valid',
      key: 'valid',
      render: (text: boolean) =>
        text ? (
          <CheckOutlined style={{ color: 'green' }} />
        ) : (
            <CloseOutlined style={{ color: 'red' }} />
          ),
    },
    {
      title: 'Mobile Carrier',
      dataIndex: 'carrier',
      key: 'carrier',
    },
    {
      title: 'Line Type',
      dataIndex: 'line_type',
      key: 'line_type',
    },
    {
      title: 'Country Code',
      dataIndex: 'country_code',
      key: 'country_code',
    },
    {
      title: 'Country Prefix',
      dataIndex: 'country_prefix',
      key: 'country_prefix',
    },
    {
      title: 'Local Format',
      dataIndex: 'local_format',
      key: 'local_format',
    },
    {
      title: 'Country Name',
      dataIndex: 'country_name',
      key: 'country_name',
    },
  ];

  const data = store.toJS();

  return (
    <div style={{ margin: '50px 0' }}>
      <CustomTable columns={columns} rowKey='id' data={data} />

    </div>
  );
});

export default ValidationTable;
