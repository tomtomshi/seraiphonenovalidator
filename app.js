const express = require('express')
const cors = require('cors')
const axios = require('axios');

const app = express(cors())
const port = 5000

app.get('/verify', (req, res) => {
    const phoneNum = req.query.phone
    if(!phoneNum){
        res.json({"message": "no phone provided"});
    }else{
        const url = `http://apilayer.net/api/validate?access_key=186f39c39229e20a13d185338654c932&format=1&number=${phoneNum}`

        axios.get(url).then(response => {
            const result = response.data;
            res.json(result);
        }).catch(error=>console.error(error));
    }
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))