## Quick Summary
Realize the business requirements of Phone No Validator riding on the technical requirements guide line.


## Technology Used

React 16.9

Node.js 10.16.3


## How to Start 

### Get the source from bitbucket:

```
git clone https://tomtomshi@bitbucket.org/tomtomshi/seraiphonenovalidator.git

```


### Local Env Build and Run

```
npm install
npm run start
```


